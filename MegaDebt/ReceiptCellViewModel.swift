//
//  ReceiptCellViewModel.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 17.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct ReceiptCellViewModel {
	enum RelationStyle: Equatable {
		case creditor(paid: Double, total: Double)
		case borrower(paid: Double, total: Double)
	}

	enum StatusStyle: Equatable {
		case open
		case pending
		case closed
	}

	let relation: RelationStyle
	let status: StatusStyle
	let title: String
	let date: String
	let id: String

	var actionHanler: (() -> Void)?
}

extension ReceiptCellViewModel: Equatable {
	static func == (lhs: ReceiptCellViewModel, rhs: ReceiptCellViewModel) -> Bool {
		return lhs.relation == rhs.relation ||
			lhs.status	== rhs.status ||
			lhs.title == rhs.title
	}
}
