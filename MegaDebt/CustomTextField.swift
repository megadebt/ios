//
//  CustomLabel.swift
//  MegaDebt
//
//  Created by Artuhay on 26.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {
  
//  override init(frame: CGRect) {
//    super.init(frame: frame)
//  }
//  convenience init() {
//    self.init()
//  }
  
//  required init?(coder: NSCoder) {
//    fatalError("init(coder:) has not been implemented")
//  }
  
  let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5)
  
   override open func textRect(forBounds bounds: CGRect) -> CGRect {
       return bounds.inset(by: padding)
   }

  override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
       return bounds.inset(by: padding)
   }

  override open func editingRect(forBounds bounds: CGRect) -> CGRect {
       return bounds.inset(by:padding)
   }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
