//
//  User.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 01.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct User: Codable {
	let id: String
	let login: String
	let mail: String
	let rating: Double
	let photo_url: String
	let display_name: String
}

extension User: Equatable { }

struct ExtendedUser: Codable {
	let id: String
	let login: String
	let mail: String
	let rating: Double
	let photo_url: String
	let display_name: String
	let admin: Bool

	func commonUser() -> User {
		return User(id: self.id, login: self.login, mail: self.mail, rating: self.rating, photo_url: self.photo_url, display_name: self.display_name)

	}
}
