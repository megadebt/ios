//
//  ReceiptListAssembly.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 08.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

final class ReceiptListAssebmly {
	static func assemble(controller: ReceiptListViewController) {
		// services
		let urlResolver = URLResolver()
		let networkService = NetworkService()
		let service = ReceiptsService(networkService: networkService, urlResolver: urlResolver)

		// vip
		let presenter = ReceiptListPresenter()
		presenter.output = controller
		let interactor = ReceiptListInteractor(service: service,
											   output: presenter)
		controller.interactor = interactor
	}
}
