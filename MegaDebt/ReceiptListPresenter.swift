//
//  ReceiptListPresenter.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

protocol ReceiptListPresenterProtocol {
	func presentReceipts(receipts: [ReceiptLight]);
	func presentHistory(receipts: [ReceiptLight]);
}

protocol ReceiptListPresenterOutput: AnyObject {
	func presentReceipts(myReceipts: [ReceiptCellViewModel],
						 participatedReceipts: [ReceiptCellViewModel])

	func presentHistory(receipts: [ReceiptCellViewModel])

	func presentReceiptDetails(receiptId: String)

	func presentError(error: Error)
}

final class ReceiptListPresenter: ReceiptListPresenterProtocol {

	weak var output: ReceiptListPresenterOutput?

	func presentReceipts(receipts: [ReceiptLight]) {
		let owned = convert(receipts: receipts.filter({ $0.owned }))
		let participated = convert(receipts: receipts.filter({ $0.owned == false }))
		output?.presentReceipts(myReceipts: owned, participatedReceipts: participated)
	}

	func presentHistory(receipts: [ReceiptLight]) {
		let viewModels = convert(receipts: receipts)
		output?.presentHistory(receipts: viewModels)
	}

	private func convert(receipts: [ReceiptLight]) -> [ReceiptCellViewModel] {
		return receipts.sorted { first, second in
			return first.date > second.date
		}.map { receipt in
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "dd.MM.yyyy"
			let dateString = dateFormatter.string(from: receipt.date)
			var state = ReceiptCellViewModel.StatusStyle.open
			switch receipt.state {
			case .open:
				state = .open
			case .in_progress:
				state = .pending
			case .closed:
				state = .closed
			}
			let relation: ReceiptCellViewModel.RelationStyle = receipt.owned ? .creditor(paid: receipt.paid_amount, total: receipt.amount)
			: .borrower(paid: receipt.paid_amount, total: receipt.amount)

			let viewModel = ReceiptCellViewModel(relation: relation,
												 status: state,
												 title: receipt.name,
												 date: dateString,
												 id: receipt.id,
												 actionHanler: { [weak self] in
													self?.output?.presentReceiptDetails(receiptId: receipt.id)
			})
			return viewModel
		}
	}
}

extension ReceiptListPresenter: ReceiptListInteractorOutputProtocol {
	func didRecieve(result: Result<[ReceiptLight], Error>) {
		switch result {
		case .success(let items):
			presentReceipts(receipts: items)
		case .failure(let error):
			output?.presentError(error: error)
		}
	}

	func didRecieveHistory(result: Result<[ReceiptLight], Error>) {
		switch result {
		case .success(let items):
			presentHistory(receipts: items)
		case .failure(let error):
			output?.presentError(error: error)
		}
	}
}
