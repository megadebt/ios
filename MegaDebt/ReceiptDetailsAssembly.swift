//
//  ReceiptDetailsAssembly.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 24.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

final class ReceiptDetailsAssembly {

	static func assemble(controller: ReceiptDetailsViewController,
						 receiptId: String) {
		// services
		let urlResolver = URLResolver()
		let networkService = NetworkService()
		let receiptsSevice = ReceiptsService(networkService: networkService, urlResolver: urlResolver)
		let friendsService = FriendsService(networkService: networkService, urlResolver: urlResolver)

		// vip
		let presenter = ReceiptDetailsPresenter()
		presenter.output = controller
		let interactor = ReceiptDetailsInteractor(receiptsService: receiptsSevice,
												  friendsService: friendsService,
												  recieptId: receiptId)
		presenter.interactor = interactor
		interactor.output = presenter
		controller.interactor = interactor
	}
}
