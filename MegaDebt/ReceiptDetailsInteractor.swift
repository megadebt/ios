//
//  ReceiptDetailsInteractor.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 24.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

protocol ReceiptDetailsInteractorProtocol: AnyObject {
	func start()
	func closeReceipt()
	func changeState(debptId: String)
}

protocol ReceiptDetailsInteractorOutputProtocol: AnyObject {
	func present(error: Error)
	func present(receiptDetail: ReceiptDetail,
				 user: ExtendedUser)
	func closeModule()
}

final class ReceiptDetailsInteractor: ReceiptDetailsInteractorProtocol {

	let receiptsService: ReceiptsServiceProtocol
	let friendsService: FriendsServiceProtocol
	let receiptId: String

	var receiptDetail: ReceiptDetail? {
		didSet {
			presentIfReady()
		}
	}
	var currentUser: ExtendedUser? {
		didSet {
			presentIfReady()
		}
	}

	var output: ReceiptDetailsInteractorOutputProtocol?

	init(receiptsService: ReceiptsServiceProtocol,
		 friendsService: FriendsServiceProtocol,
		 recieptId: String) {
		self.receiptsService = receiptsService
		self.friendsService	= friendsService
		self.receiptId = recieptId
	}
	
	func start() {
		receiptsService.getReceipt(receiptId: receiptId) { [weak self] result in
			do {
				self?.receiptDetail = try result.get()
			} catch let error {
				DispatchQueue.main.async {
					self?.output?.present(error: error)
				}
			}
		}
		friendsService.getUserInfo { [weak self] result in
			do {
				self?.currentUser = try result.get()
			} catch let error {
				DispatchQueue.main.async {
					self?.output?.present(error: error)
				}
			}
		}
	}

	func closeReceipt() {
		receiptsService.closeReceipt(receiptId: receiptId) { [weak self] result in
			do {
				_ = try result.get()
				self?.output?.closeModule()
			} catch let error {
				DispatchQueue.main.async {
					self?.output?.present(error: error)
				}
			}
		}
	}

	func changeState(debptId: String) {
		receiptsService.changeDebptState(debptId: debptId) { [weak self] result in
			do {
				_ = try result.get()
				self?.currentUser = nil
				self?.receiptDetail = nil
				self?.start()
			} catch let error {
				DispatchQueue.main.async {
					self?.output?.present(error: error)
				}
			}
		}
	}

	private func presentIfReady() {
		guard let currentUser = currentUser,
			let receiptDetail = receiptDetail else {
				return
		}

		DispatchQueue.main.async { [weak self] in
			self?.output?.present(receiptDetail: receiptDetail,
								  user: currentUser)
		}
	}
}
