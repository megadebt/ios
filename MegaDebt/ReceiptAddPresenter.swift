//
//  ReceiptAddPresenter.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 17.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import RxDataSources

protocol ReceiptAddPresenterOutputProtocol: AnyObject {

	func presentPositionDetails(position: ReceiptAdd.Item?)

	func presentSections(sectionModels: [SectionModel<String, ReceiptAddCellModel>])

	func presentError(error: Error)

	func close()
}

final class ReceiptAddPresenter {

	weak var interactor: ReceiptAddInteractorProtocol?
	let output: ReceiptAddPresenterOutputProtocol

	init(output: ReceiptAddPresenterOutputProtocol) {
		self.output = output
	}

}
extension ReceiptAddPresenter: ReceiptAddInteractorOutputProtocol {
	func present(model: ReceiptAdd) {
		let generalSection = SectionModel<String, ReceiptAddCellModel>(model: "General", items: [
			.textField(text: model.name, placeHolder: "Name") { [weak interactor] newValue in
				interactor?.set(name: newValue)
			},
			.textField(text: model.description, placeHolder: "Description") { [weak interactor] newValue in
				interactor?.set(description: newValue)
			}
		])

		let positions = model.items.map { item in
			return ReceiptAddCellModel.position(name: item.name,
												count: item.quantity,
												total: item.price * Double(item.quantity),
												action: { [weak self] in
													self?.output.presentPositionDetails(position: item)
			})
		}

		let addPosition = ReceiptAddCellModel.addPositionButton(action: { [weak self] in
			self?.output.presentPositionDetails(position: nil)
		})

		let positionsSection = SectionModel(model: "Positions", items: positions + [addPosition])

		let totalSum = model.items.reduce(0.0) { currentSum, item in
			return currentSum + item.price * Double(item.quantity)
		}

		let canSave = model.items.isEmpty == false &&
			model.name.isEmpty == false &&
			model.description.isEmpty == false

		let summarySection = SectionModel<String, ReceiptAddCellModel>(model: "Summary", items: [
			.total(total: totalSum),
			.saveButton(isActive: canSave, title: "Save Receipt") { [weak interactor] in
				interactor?.saveReceipt()
			}
		])

		output.presentSections(sectionModels: [generalSection, positionsSection, summarySection])
	}

	func didRecieve(result: Result<ReceiptAddResponse, Error>) {
		switch result {
		case .success(_):
			output.close()
		case .failure(let error):
			output.presentError(error: error)
		}
	}
}
