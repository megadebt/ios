//
//  BigButtonCell.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 08.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

final class BigButtonCell: UITableViewCell {

	@IBOutlet weak var button: UIButton!

	var actionHandler: (() -> Void)?

	@IBAction func didTap(_ sender: Any) {
		actionHandler?()
	}
}
