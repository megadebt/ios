//
//  CreateMeetingViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 14.04.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class CreateMeetingViewController: UIViewController {
    
    private let textFieldHeight : CGFloat = 50
    private let leadingConstant: CGFloat = 10
    private let trailingConstant : CGFloat = -10
    private let betweenConstant : CGFloat = 10
    
    var meetingNameTextField : UITextField!
    var moneyTextField : UITextField!
    
    var members : UIView!
    
    var addButton : UIButton!
    
    func setMeetingNameTextField() {
        meetingNameTextField = UITextField()
        self.view.addSubview(meetingNameTextField)
        
        meetingNameTextField.translatesAutoresizingMaskIntoConstraints = false
        meetingNameTextField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        meetingNameTextField.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        meetingNameTextField.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        meetingNameTextField.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        
        meetingNameTextField.placeholder = "Meeting Name"
    }
    
    func setMoneyTextField() {
        moneyTextField = UITextField()
        self.view.addSubview(moneyTextField)
        
        moneyTextField.translatesAutoresizingMaskIntoConstraints = false
        moneyTextField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        moneyTextField.topAnchor.constraint(equalTo: meetingNameTextField.bottomAnchor, constant: betweenConstant).isActive = true
        moneyTextField.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        moneyTextField.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        
        moneyTextField.placeholder = "Money"
    }
    
    func setAddButton() {
        addButton = UIButton()
        self.view.addSubview(addButton)
        
        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        addButton.leadingAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        addButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        addButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        addButton.backgroundColor = .purple
        addButton.setTitle("Add meeting", for: .normal)
        
        addButton.showsTouchWhenHighlighted = true
    }
    
    func setView() {
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
        } else {
            self.view.backgroundColor = .white
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setMeetingNameTextField()
        setMoneyTextField()
        setAddButton()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
