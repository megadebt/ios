//
//  ReceiptListInteractor.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

protocol ReceiptListInteractorProtocol {
	func getReceipts() -> Void

	func getReceiptsHistory() -> Void
}

protocol ReceiptListInteractorOutputProtocol {
	func didRecieve(result: Result<[ReceiptLight], Error>)

	func didRecieveHistory(result: Result<[ReceiptLight], Error>)
}

final class ReceiptListInteractor: ReceiptListInteractorProtocol {

	let service: ReceiptsServiceProtocol
	let output: ReceiptListInteractorOutputProtocol

	init(service: ReceiptsServiceProtocol,
		 output: ReceiptListInteractorOutputProtocol) {
		self.service = service
		self.output = output
	}

	func getReceipts() {
		service.getReceipts { [weak self] result in
			DispatchQueue.main.async {
				self?.output.didRecieve(result: result)
			}
		}
	}

	func getReceiptsHistory() -> Void {
		service.getReceiptsHistory { [weak self] result in
			DispatchQueue.main.async {
				self?.output.didRecieve(result: result)
			}
		}
	}
}
