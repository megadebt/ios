//
//  TotalCell.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 15.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

final class TotalCell: UITableViewCell {

	@IBOutlet private weak var totalLabel: UILabel!

	func set(total: Double) {
		totalLabel.text = "Total: \(total)"
	}
}
