//
//  ProfileViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 30.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import Alamofire
import CocoaDebug
import Cosmos

protocol ProfileViewControllerDelegate {
	func getGoogleIdInfo(profileModel: ProfileModel)
}

private var vSpinner : UIView?

class ProfileViewController: UIViewController {

	let service : FriendsServiceProtocol = FriendsService(networkService: NetworkService(), urlResolver: URLResolver())


	@IBOutlet weak var UserAvatar: UIImageView!
	@IBOutlet weak var userFullName: UILabel!
	@IBOutlet weak var eMail: UILabel!
	@IBOutlet weak var signIn: UIButton!
	@IBOutlet weak var signUp: UIButton!
	@IBOutlet weak var friendsButton: UIButton!
	@IBOutlet weak var userLogin: UILabel!
	@IBOutlet weak var logsButton: UIButton!
	@IBOutlet weak var ratingView: CosmosView!

	@IBAction func friendsButtonAction(_ sender: Any) {
		performSegue(withIdentifier: "friendsSegue", sender: sender)
	}


	@IBAction func actionLogIn(_ sender: Any) {
		if (UserDefaults.standard.bool(forKey: "isLogIn")) {
			UserDefaults.standard.set(false, forKey: "isLogIn")
			let firebaseAuth = Auth.auth()
			do {
				try firebaseAuth.signOut()
			} catch let signOutError as NSError {
				print ("Error signing out: %@", signOutError)
			}
			makeLogOut()
		} else {
			performSegue(withIdentifier: "AuthVC", sender: sender)
		}
	}


	@IBAction func actionSignIn(_ sender: Any) {
		performSegue(withIdentifier: "RegisterVC", sender: sender)
	}

	override func viewWillAppear(_ animated: Bool) {
		Auth.auth().currentUser?.getIDToken(completion: {
			token, error in
			//print(token)

		})
		//print(token)
		super.viewWillAppear(animated)
		if (UserDefaults.standard.bool(forKey: "isLogIn")) {
			makeLogIn()
		} else {
			makeLogOut()
		}
		print(UserDefaults.standard.string(forKey: "Token"))

	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "AuthVC" {
			let controller = segue.destination as! AuthViewController
			controller.profileVCDelegate = self
		} else if segue.identifier == "RegisterVC" {
			let controller = segue.destination as! RegisterViewController
			controller.profileVCDelegate = self
		}
	}

	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/

	func makeLogOut() {
		signIn.setTitle("Log in", for: .normal)
		signUp.isHidden = false
		UserAvatar.isHidden = true
		UserAvatar.image = UIImage(named: "UserAvatarSample")
		ratingView.isHidden = true
		userFullName.isHidden = true
		userFullName.text = ""
		eMail.isHidden = true
		eMail.text = ""
		userLogin.isHidden = true
		userLogin.text = ""
		friendsButton.isHidden = true
		UserDefaults.standard.setValue("", forKey: "Token")
		UserDefaults.standard.setValue("", forKey: "UserID")
	}

	func makeLogIn() {
		signIn.titleLabel?.isHidden = false
		signIn.setTitle("Log out", for: .normal)
		signUp.isHidden = true
		UserAvatar.isHidden = false
		ratingView.isHidden = false
		userFullName.isHidden = false
		userLogin.isHidden = false
		eMail.isHidden = false
		friendsButton.isHidden = false
		getUserInfo()
		//loadFromGoogleAcc()
	}

	func getUserInfo() {
		self.showSpinner(onView: self.view)
		service.getUserInfo(completionHandler: {[weak self] result in
			DispatchQueue.main.async {
				self?.updateFields(result: result)
				self?.removeSpinner()
			}})
	}

	func updateFields(result: Result<ExtendedUser, Error>) {
		switch result {
		case .success(let item):
			userFullName.text = "Full Name: \(item.display_name)"
			userLogin.text = "Login: \(item.login)"
			ratingView.rating = item.rating
//			userRating.text = "Rating: \(item.rating)"
			eMail.text = "Email: \(item.mail)"
			if item.admin {
				logsButton.isHidden = false
			}

			let imgUrl = URL(string: item.photo_url)
			if let url = imgUrl {
				if let data = try? Data(contentsOf: url) {
					if let image = UIImage(data: data) {
						UserAvatar.image = image
					}
				}
			}
			print("Success to load user info!")
		case .failure(let error):
			print(error)
		}
	}


	func loadFromGoogleAcc() {
		if let googleCurUser =  GIDSignIn.sharedInstance()?.currentUser {
			let imgUrl = googleCurUser.profile.imageURL(withDimension: 240)
			var img = UIImage()
			if let data = try? Data(contentsOf: imgUrl!) {
				if let image = UIImage(data: data) {
					img = image
				}
			}
			let profile = ProfileModel(name: googleCurUser.profile.name, email: googleCurUser.profile.email, avatar: img)
			getGoogleIdInfo(profileModel: profile)
		}
		if let curUser = Auth.auth().currentUser {
			let profile = ProfileModel(name: curUser.displayName!, email: curUser.email!)
			getGoogleIdInfo(profileModel: profile)
		}
	}

	@IBAction func logsButtonDidTap(_ sender: Any) {
		CocoaDebug.showBubble()
	}
}

extension ProfileViewController: ProfileViewControllerDelegate {
	func getGoogleIdInfo(profileModel: ProfileModel) {
		if let rating = profileModel.rating {
			ratingView.rating = Double(rating)
		}
		if let avatar = profileModel.avatar {
			UserAvatar.image = avatar
		}
		//    if let username = profileModel.username {
		//      userName.text = username
		//    }
		userFullName.text = profileModel.name
		eMail.text = profileModel.email

	}
}


extension UIViewController {

	func showSpinner(onView : UIView) {
		let spinnerView = UIView.init(frame: onView.bounds)
		spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
		let ai = UIActivityIndicatorView.init(style: .whiteLarge)
		ai.startAnimating()
		ai.center = spinnerView.center

		DispatchQueue.main.async {
			spinnerView.addSubview(ai)
			onView.addSubview(spinnerView)
		}

		vSpinner = spinnerView
	}

	func removeSpinner() {
		DispatchQueue.main.async {
			vSpinner?.removeFromSuperview()
			vSpinner = nil
		}
	}
}
