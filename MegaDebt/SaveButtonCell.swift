//
//  SaveButtonCell.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 08.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

final class SaveButtonCell: UITableViewCell {
	@IBOutlet weak var button: UIButton!

	var actionHandler: (() -> Void)?
	var title: String? {
		get {
			return button.titleLabel?.text
		}

		set {
			button.titleLabel?.text = newValue
		}
	}

	@IBAction func didTap(_ sender: Any) {
		actionHandler?()
	}
}

