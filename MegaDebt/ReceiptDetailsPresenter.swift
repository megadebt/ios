//
//  ReceiptDetailsPresenter.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 24.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import RxDataSources

protocol ReceiptDetailsPresenterOutputProtocol: AnyObject {

	func presentError(error: Error)

	func presentSections(sectionModels: [SectionModel<String, ReceiptDetailsCellModel>],
						 receiptName: String,
						 receiptDate: String,
						 receiptState: String,
						 receiptOwner: String,
						 receiptAmount: String,
						 receiptPaidAmount: String)

	func closeModule()
}

final class ReceiptDetailsPresenter {

	weak var output: ReceiptDetailsPresenterOutputProtocol?
	weak var interactor: ReceiptDetailsInteractorProtocol?

	private func createPositionsSectionModel(receiptDetail: ReceiptDetail) -> SectionModel<String, ReceiptDetailsCellModel> {
		let items = receiptDetail.items

		let model = SectionModel(model: "Positions", items: items.map { item in
			return ReceiptDetailsCellModel.position(name: item.name, count: item.quantity, total: Double(item.price) * Double(item.quantity))
			}
		)
		return model
	}

	private func createDebtorsSecetionModel(receiptDetail: ReceiptDetail, user: User) -> SectionModel<String, ReceiptDetailsCellModel> {
		let debts = receiptDetail.debts

		return SectionModel(model: "Debtors", items: debts.map { debt in
			var avatar: UIImage?
			if let url = URL(string: debt.borrower.photo_url) {
				avatar = try? UIImage(data: Data(contentsOf: url))
			}
			var changeActionHandler: (() -> Void)? = { [weak self] in
				self?.interactor?.changeState(debptId: debt.id)
			}

			if user != receiptDetail.owner,
				debt.debtor != user {
				changeActionHandler = nil
			}

			if debt.debtor == user,
				debt.state == .pending {
				changeActionHandler = nil
			}

			if debt.state == .closed {
				changeActionHandler = nil
			}

			return ReceiptDetailsCellModel.debtor(name: debt.debtor.display_name == "" ? debt.debtor.login : debt.debtor.display_name,
												  avatar: avatar,
												  state: String(debt.state.rawValue),
												  amount: String(format:"%.2f ₽", debt.amount),
												  changeStateHandler: changeActionHandler)
		})
	}

	private func createManagingSectionModel() -> SectionModel<String, ReceiptDetailsCellModel> {
		return SectionModel(model: "Managing", items: [
			ReceiptDetailsCellModel.closeReceiptButton(action: { [ weak self] in
				self?.interactor?.closeReceipt()
			})
		])
	}
}

extension ReceiptDetailsPresenter: ReceiptDetailsInteractorOutputProtocol {
	func closeModule() {
		output?.closeModule()
	}

	func present(receiptDetail: ReceiptDetail,
				 user: ExtendedUser) {

		let positions = createPositionsSectionModel(receiptDetail: receiptDetail)
		let debtors = createDebtorsSecetionModel(receiptDetail: receiptDetail, user: user.commonUser())
		var allSections = [positions, debtors]
		if receiptDetail.owner == user.commonUser(),
			receiptDetail.state != .closed {
			allSections.append(createManagingSectionModel())
		}

		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd.MM.yyyy"

		output?.presentSections(sectionModels: allSections,
								receiptName: receiptDetail.name,
								receiptDate: dateFormatter.string(from: receiptDetail.date),
								receiptState: receiptDetail.state.rawValue,
								receiptOwner: receiptDetail.owner.login,
								receiptAmount: String(format: "%.2f", receiptDetail.amount),
								receiptPaidAmount: String(format: "%.2f", receiptDetail.paid_amount))
	}

	func present(error: Error) {
		output?.presentError(error: error)
	}
}
