//
//  ProfieTableViewController.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 17.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import RxDataSources
import RxSwift

final class ProfileTableViewController: UITableViewController {

	let data = Observable<[String]>.just(["Иван", "Петя", "Вася", "Коля"])

	override func viewDidLoad() {
		super.viewDidLoad()
		setup()
	}

	private func setup() {
		tableView.rowHeight = 44.0
		refreshControl?.addTarget(self, action: #selector(refreshStarted), for: .valueChanged)

		data.bind(to: tableView.rx.items(cellIdentifier: "right_detail")) { index, model, cell in
			cell.textLabel?.text = "Имя"
			cell.detailTextLabel?.text = model
		}
		.dispose()
	}

	@objc func refreshStarted() {
		DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { [weak self] in
			self?.refreshControl?.endRefreshing()
		}
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 {
			return 4
		}
		return 0
	}

	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return "Персональные данные"
	}
}
