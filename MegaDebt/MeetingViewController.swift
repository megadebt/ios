//
//  ViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 14.04.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class MeetingViewController: UIViewController {
    
    var meetingTable : UITableView!
    
    var meetingData = [NSObject]()
    
    func configureMeetingTableView() {
        meetingTable = UITableView()
        self.view.addSubview(meetingTable)
        
        meetingTable.translatesAutoresizingMaskIntoConstraints = false
        meetingTable.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        meetingTable.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        meetingTable.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        meetingTable.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
    }
    
    func configureView() {
        meetingTable.dataSource = self
        meetingTable.delegate = self
        self.title = "Main Screen"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addMeeting))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "UserButtonBlack"), style: .plain, target: self, action: #selector(moveToUserScreen))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureMeetingTableView()
        configureView()
        // Do any additional setup after loading the view.
    }

    @objc func moveToUserScreen() {
        self.navigationController?.pushViewController(UserViewController(), animated: true)
    }
    
    @objc func addMeeting() {
        self.present(CreateMeetingViewController(), animated: true, completion: nil)
    }

}

extension MeetingViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meetingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

