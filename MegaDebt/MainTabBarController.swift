//
//  MainTabBarController.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 17.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

final class MainTabBarController: UITabBarController {

	override func viewDidLoad() {
		super.viewDidLoad()
		selectReceiptsViewController()
	}

	func selectReceiptsViewController() {
		selectedIndex = 1
	}
}
