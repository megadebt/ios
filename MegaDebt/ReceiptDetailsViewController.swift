//
//  ReceiptDetailsViewController.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 18.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

enum ReceiptDetailsCellModel {
	case debtor(name: String,
		avatar: UIImage?,
		state: String,
		amount: String,
		changeStateHandler: (() -> Void)?)
	case closeReceiptButton(action: (() -> Void)?)
	case position(name: String,
		count: Int,
		total: Double)
}


final class ReceiptDetailsViewController: UIViewController {
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var stateLabel: UILabel!
	@IBOutlet weak var ownerLabel: UILabel!
	@IBOutlet weak var amountLabel: UILabel!
	@IBOutlet weak var paidAmountLabel: UILabel!

	@IBOutlet weak var tableView: UITableView!

	let disposeBag = DisposeBag()
	lazy var items = PublishSubject<[SectionModel<String, ReceiptDetailsCellModel>]>()

	let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, ReceiptDetailsCellModel>>(configureCell: {
		(dataSource, table, indexPath, cellViewModel) in
		switch cellViewModel {
		case let .debtor(name, avatar, state, amount, changeStateHandler):
			let cell = table.dequeueReusableCell(withIdentifier: "DebtorCell") as! DebtorCell
			if let avatar = avatar {
				cell.avatarImageView.image = avatar
			}
			cell.nameLabel.text = name
			cell.stateLabel.text = state.uppercased()
			cell.debtAmoutLabel.text = amount

			if let handler = changeStateHandler {
				cell.debtChangeStateButton.isEnabled = true
				cell.changeStateHandler = handler
			} else {
				cell.debtChangeStateButton.isEnabled = false
			}
			return cell
		case let .position(name, count, total):
			let cell = table.dequeueReusableCell(withIdentifier: "PositionCell") as! PositionCell
			let viewModel = PositionCellViewModel(name: name, count: count, total: total)
			cell.set(viewModel: viewModel)
			return cell
		case let .closeReceiptButton(action):
			let cell = table.dequeueReusableCell(withIdentifier: "BigButtonCell") as! BigButtonCell
			if let action = action {
				cell.button.isEnabled = true
				cell.actionHandler = action
			} else {
				cell.button.isEnabled = false
			}
			return cell
		}
	})

	var interactor: ReceiptDetailsInteractorProtocol?
	var receiptId: String?

	override func viewDidLoad() {
		super.viewDidLoad()
		configureTableViewBinding()
		configureRefreshControl()
		assemble()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		tableView.refreshControl?.beginRefreshing()
		interactor?.start()
	}

	private func assemble() {
		guard let receiptId = receiptId else { return }
		ReceiptDetailsAssembly.assemble(controller: self, receiptId: receiptId)
	}

	private func configureTableViewBinding() {
		dataSource.titleForHeaderInSection = { dataSource, index in
			return dataSource.sectionModels[index].model
		}

		items.bind(to: tableView.rx.items(dataSource: dataSource)).disposed(by: disposeBag)
	}

	private func configureRefreshControl() {
		tableView.refreshControl = UIRefreshControl()
	}
}

extension ReceiptDetailsViewController: ReceiptDetailsPresenterOutputProtocol {

	func presentError(error: Error) {
		presentAlertWith(error: error)
		tableView.refreshControl?.endRefreshing()
	}

	func presentSections(sectionModels: [SectionModel<String, ReceiptDetailsCellModel>], receiptName: String, receiptDate: String, receiptState: String, receiptOwner: String, receiptAmount: String, receiptPaidAmount: String) {
		items.onNext(sectionModels)
		nameLabel.text = "Name: \(receiptName)"
		dateLabel.text = "Date: \(receiptDate)"
		stateLabel.text = "State: \(receiptState)"
		ownerLabel.text = "Owner: \(receiptOwner)"
		amountLabel.text = "Amount: \(receiptAmount) ₽"
		paidAmountLabel.text = "Paid: \(receiptPaidAmount) ₽"
		tableView.refreshControl?.endRefreshing()
		tableView.refreshControl = nil
	}

	func closeModule() {
		navigationController?.popViewController(animated: true)
	}
}
