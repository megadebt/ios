//
//  FindFriendViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 18.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class FindFriendViewController: UIViewController {
  
  func getUserLogin(users: [User]) {
    for u in users {
      if u.login == frinedLoginTextField.text {
        
        service.addFriend(id: u.id, completionHandler: { [weak self] result in
          self?.navigationController?.popViewController(animated: true)
        })
        break
      }
    }
  }
  
  func getUsers(result: Result<[User], Error>) {
    switch result {
    case .success(let items):
      getUserLogin(users: items)
			if let fullLogin = frinedLoginTextField.text {
				let alert = UIAlertController(title: "Friend request send is success!", message: "User with login: \(fullLogin) is found.", preferredStyle: .alert)
				alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
				present(alert, animated: true)
		}
    case .failure(let item):
			if let fullLogin = frinedLoginTextField.text {
				let alert = UIAlertController(title: "Friend request send is failed!", message: "User with login: \(fullLogin) is not found.", preferredStyle: .alert)
			
				alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
				present(alert, animated: true)
			}
      print(item)
      print("Failed to load users!")
    }
  }
  
   let service : FriendsServiceProtocol = FriendsService(networkService: NetworkService(), urlResolver: URLResolver())
  
  @IBOutlet weak var frinedLoginTextField: UITextField!
  
  @IBAction func findFriendByLoginTap(_ sender: Any) {
    if let login = frinedLoginTextField.text {
    service.getUserInfoByLogin(invitedLogin: login, completionHandler: { [weak self] result in
      DispatchQueue.main.async {
        self?.getUsers(result: result)
      } 
    })
  }
  }
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
