//
//  FriendViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 14.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import Cosmos

private var vSpinner : UIView?


class FriendViewController: UIViewController {
	var userId : String!
	
	let service : FriendsServiceProtocol = FriendsService(networkService: NetworkService(), urlResolver: URLResolver())

	@IBOutlet weak var friendAvatar: UIImageView!
	@IBOutlet weak var friendFullName: UILabel!
	@IBOutlet weak var friendEmail: UILabel!
	@IBOutlet weak var friendNickName: UILabel!
	@IBOutlet weak var ratingView: CosmosView!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}

	
	override func viewWillAppear(_ animated: Bool) {
		service.getUserInfoById(id: userId, completionHandler: {[weak self] result in
			DispatchQueue.main.async {

				self?.updateFields(result: result)
			}
		})
		makeLogIn()
	}
	
	func makeLogOut() {
		friendAvatar.isHidden = true
		friendAvatar.image = UIImage(named: "UserAvatarSample")
		ratingView.isHidden = true
		friendFullName.isHidden = true
		friendFullName.text = ""
		friendEmail.isHidden = true
		friendEmail.text = ""
		friendNickName.isHidden = true
		friendNickName.text = ""
	}

	func makeLogIn() {
		friendAvatar.isHidden = false
		ratingView.isHidden = false
		friendFullName.isHidden = false
		friendNickName.isHidden = false
		friendEmail.isHidden = false
	}

	//  func getUserInfo() {
	//    self.showSpinner(onView: self.view)
	//    service.getUserInfo(completionHandler: {[weak self] result in
	//   DispatchQueue.main.async {
	//     self?.updateFields(result: result)
	//    self?.removeSpinner()
	//   }})
	//  }
	func getUserID(result: Result<User, Error>) {
		switch result {
		case .success(let items):

			break
		case .failure(_):
			print("Failed to load friends Id!")
		}
	}
	
	func updateFields(result: Result<User, Error>) {
		switch result {
		case .success(let item):
			friendFullName.text = "Full Name: \(item.display_name)"
			friendNickName.text = "Login: \(item.login)"
			ratingView.rating = item.rating
			friendEmail.text = "Email: \(item.mail)"
			let imgUrl = URL(string: item.photo_url)
			if let url = imgUrl {
				if let data = try? Data(contentsOf: url) {
					if let image = UIImage(data: data) {
						friendAvatar.image = image
					}
				}
			}
			print("Success to load user info!")
		case .failure(let error):
			print(error)
		}
	}
	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/

}
