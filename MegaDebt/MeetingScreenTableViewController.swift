//
//  MeetingScreenTableViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 25.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import Alamofire

protocol MeetingViewControllerDelegate {
  func addMeetingItems(item: ItemObject)
}

class MeetingScreenTableViewController: UIViewController {
  
  var meetingItems = [ItemObject]()
  
  let meetingName: CustomTextField = {
    let tf = CustomTextField()
    if #available(iOS 13.0, *) {
      tf.backgroundColor = .systemBackground
    } else {
      tf.backgroundColor = .white
      // Fallback on earlier versions
    }
    tf.placeholder = "Input meeting name"
    tf.returnKeyType = .done
    tf.translatesAutoresizingMaskIntoConstraints = false
    //tf.layer.borderColor = UIColor.red.cgColor
    return tf
  }()
  
  let positionsTableView : UITableView = {
    let table = UITableView()
    table.translatesAutoresizingMaskIntoConstraints = false
    table.register(ItemTableViewCell.self, forCellReuseIdentifier: "ItemTableViewCell")
    return table
  }()

  func addMeetingNameTextFieldConstraints() {
    let safeArea = view.safeAreaLayoutGuide
    view.addSubview(meetingName)
    meetingName.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
    meetingName.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
    meetingName.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
    meetingName.heightAnchor.constraint(equalToConstant: 50).isActive = true
    meetingName.delegate = self
  }
  
  func addTableViewConstraints() {
    let safeArea = view.safeAreaLayoutGuide
    view.addSubview(positionsTableView)
    positionsTableView.topAnchor.constraint(equalTo: meetingName.bottomAnchor, constant: 0).isActive = true
    positionsTableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: 0).isActive = true
    positionsTableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 0).isActive = true
    positionsTableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: 0).isActive = true
    
    positionsTableView.delegate = self
    positionsTableView.dataSource = self
    
  }
  
  func setupView() {
//    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneFunc))
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addItem))
  }
  override func viewDidLoad() {
    modalPresentationStyle = .fullScreen
    super.viewDidLoad()
    setupView()
    addMeetingNameTextFieldConstraints()
    addTableViewConstraints()
      // Do any additional setup after loading the view.
  }
    
  @objc func doneFunc() {
    self.dismiss(animated: true, completion: nil)
  }
  
  @objc func addItem() {
    let addItemVC = AddItemViewController()
    addItemVC.createMeetingDelegate = self
    present(addItemVC, animated: true, completion: nil)
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MeetingScreenTableViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return meetingItems.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as! ItemTableViewCell
    let curItem = meetingItems[indexPath.row]
    cell.itemNameLabel.text = curItem.itemName
    cell.totalPrice.text = String(curItem.itemPrice*curItem.itemQuantity)
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50
  }
  
  func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
      self.view.endEditing(true)
      return true
  }
}

extension MeetingScreenTableViewController: MeetingViewControllerDelegate {
  func addMeetingItems(item: ItemObject) {
    self.meetingItems.append(item)
    positionsTableView.reloadData()
  }
}

