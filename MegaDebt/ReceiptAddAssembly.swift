//
//  ReceiptAddAssembly.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 17.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

final class ReceiptAddAssembly {

	static func assemble(controller: ReceiptAddViewController) {
		let networkService = NetworkService()
		let urlResolver = URLResolver()
		let receiptsService = ReceiptsService(networkService: networkService,
											  urlResolver: urlResolver)

		let presenter = ReceiptAddPresenter(output: controller)
		let interactor = ReceiptAddInteractor(service: receiptsService,
											  output: presenter)
		presenter.interactor = interactor
		controller.interactor = interactor
	}
}
