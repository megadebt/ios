//
//  FriendSelectCell.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 18.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

final class FriendSelectCell: UITableViewCell {
	@IBOutlet weak var avatarImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!

	var actionBlock: ((Bool) -> Void)?
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setupSelection()
	}

	private func setupSelection() {
		let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchSelection))
		addGestureRecognizer(gestureRecognizer)
	}

	@objc func switchSelection() {
		if accessoryType == .checkmark {
			accessoryType = .none
		} else {
			accessoryType = .checkmark
		}
		actionBlock?(accessoryType == .checkmark)
	}
}
