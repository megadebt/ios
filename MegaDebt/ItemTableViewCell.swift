//
//  ItemTableViewCell.swift
//  MegaDebt
//
//  Created by Artuhay on 25.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
  
  var itemNameLabel : UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  var totalPrice : UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  func addConstraints() {
    addSubview(itemNameLabel)
    addSubview(totalPrice)
    itemNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
    itemNameLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    itemNameLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
    itemNameLabel.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.width/2).isActive = true
    totalPrice.leftAnchor.constraint(equalTo: itemNameLabel.rightAnchor).isActive = true
    totalPrice.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    totalPrice.topAnchor.constraint(equalTo: topAnchor).isActive = true
    totalPrice.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
  }
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    addConstraints()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
