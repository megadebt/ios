//
//  AuthViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 30.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class AuthViewController: UIViewController, GIDSignInDelegate {

	var profileVCDelegate : ProfileViewControllerDelegate?

	@IBOutlet weak var emailTF: UITextField!

	@IBOutlet weak var passwordTF: UITextField!

	@IBAction func logInAction(_ sender: Any) {
		DispatchQueue.global(qos: .userInitiated).sync {
			Auth.auth().signIn(withEmail: emailTF.text!, password: passwordTF.text!, completion: { [weak self] authResult, error in
				guard let self = self else { return }

				guard error == nil else {
					self.presentAlertWith(error: error!)
					return
				}
				if let authResult = authResult,
					let userName = authResult.user.email {
					UserDefaults.standard.set(true, forKey: "isLogIn")
					UserDefaults.standard.setValue(userName, forKey: "UserID")
					authResult.user.getIDToken { token, error in
						guard let token = token else { return }
						UserDefaults.standard.setValue(token, forKey: "Token")
						AuthService.shared.didSignIn()
						self.navigationController?.popViewController(animated: true)
						self.tabBarController?.selectedIndex = 1
						self.profileVCDelegate?.getGoogleIdInfo(profileModel: ProfileModel(name: "Full Name", email: self.emailTF.text!))
					}
				}
			})
		}
	}

	@IBAction func googleSignIn(_ sender: Any) {
		GIDSignIn.sharedInstance().signIn()
	}
	override func viewDidLoad() {
		super.viewDidLoad()
		GIDSignIn.sharedInstance()?.presentingViewController = self
		GIDSignIn.sharedInstance().delegate = self
		// Do any additional setup after loading the view.
	}

	func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
		if let error = error {
			// ...
      print(error)
			return
		}

		guard let authentication = user.authentication else { return }
		let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
		Auth.auth().signIn(with: credential) { result, error in
      let user: GIDGoogleUser = signIn.currentUser
//			UserDefaults.standard.setValue(authentication.idToken, forKey: "Token")
//			UserDefaults.standard.setValue(user.profile.email, forKey: "UserID")
//			UserDefaults.standard.setValue(authentication.accessToken, forKey: "AuthToken")
//			AuthService.shared.didSignIn()
//			let imgUrl = user.profile.imageURL(withDimension: 240)
//			var img = UIImage()
//			if let data = try? Data(contentsOf: imgUrl!) {
//				if let image = UIImage(data: data) {
//					img = image
//				}
//			}
      Auth.auth().signIn(with: credential, completion: { [weak self] authResult, error in
        guard let self = self else { return }

        guard error == nil else {
          self.presentAlertWith(error: error!)
          return
        }
        if let authResult = authResult,
          let userName = authResult.user.email {
          UserDefaults.standard.set(true, forKey: "isLogIn")
          UserDefaults.standard.setValue(userName, forKey: "UserID")
          print(authResult.user.getIDToken)   
          authResult.user.getIDToken { token, error in
            guard let token = token else { return }
            UserDefaults.standard.setValue(token, forKey: "Token")
            AuthService.shared.didSignIn()
            self.navigationController?.popViewController(animated: true)
						self.tabBarController?.selectedIndex = 1
            self.profileVCDelegate?.getGoogleIdInfo(profileModel: ProfileModel(name: user.profile.name, email: user.profile.email))
          }
        }
      })
			//UserDefaults.standard.set(true, forKey: "isLogIn")
//			self.profileVCDelegate?.getGoogleIdInfo(profileModel: ProfileModel(name: user.profile.name, email: user.profile.email))
			//self.navigationController?.popViewController(animated: true)
		}
	}
}
