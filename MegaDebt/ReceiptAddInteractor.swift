//
//  ReceiptAddInteractor.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 17.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

protocol ReceiptAddInteractorProtocol: AnyObject {

	func set(name: String)

	func set(description: String)

	func addPosition(name: String,
					 quantity: Int,
					 price: Double,
					 participants: [String])

	func removePosition(at index: Int)

	func saveReceipt()

	func fetch()
}

protocol ReceiptAddInteractorOutputProtocol {

	func present(model: ReceiptAdd)

	func didRecieve(result: Result<ReceiptAddResponse, Error>)

}

final class ReceiptAddInteractor: ReceiptAddInteractorProtocol {

	var output: ReceiptAddInteractorOutputProtocol

	var receiptAddModel = ReceiptAdd(name: "",
									 date: Date(),
									 description: "",
									 items: [],
									 participants: []) {
		didSet {
			output.present(model: receiptAddModel)
		}
	}

	let service: ReceiptsService

	init(service: ReceiptsService,
		 output: ReceiptAddInteractorOutputProtocol) {
		self.service = service
		self.output = output
	}

	func set(name: String) {
		receiptAddModel.name = name
	}

	func set(description: String) {
		receiptAddModel.description = description
	}

	func addPosition(name: String, quantity: Int, price: Double, participants: [String]) {
		receiptAddModel.items.append(ReceiptAdd.Item(name: name,
													 quantity: quantity,
													 price: price,
													 participants: participants))
	}

	func saveReceipt() {
		receiptAddModel.participants = receiptAddModel.items.reduce(into: [String]()) { array, item in
			array.append(contentsOf: item.participants)
		}

		receiptAddModel.participants = Array(Set(receiptAddModel.participants))

		service.addReceipt(receipt: receiptAddModel) { [weak self] result in
			DispatchQueue.main.async {
				self?.output.didRecieve(result: result)
			}
		}
	}

	func removePosition(at index: Int) {
		receiptAddModel.items.remove(at: index)
	}

	func fetch() {
		output.present(model: receiptAddModel)
	}
}

extension ReceiptAddInteractor: PositionDetailsViewControllerDelegate {
	func didAddPosition(position: ReceiptAdd.Item) {
		receiptAddModel.items.append(position)
	}

	func didChangePosition(position: ReceiptAdd.Item) {
		return
	}
}
