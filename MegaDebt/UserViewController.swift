//
//  UserViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 14.04.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    
    private let labelHeight : CGFloat = 50
    private let leadingConstant: CGFloat = 10
    private let trailingConstant : CGFloat = -10
    private let betweenLabelConstant : CGFloat = 10
    
    var userName : UILabel!
    var eMail : UILabel!
    var firstNameSecondName : UILabel!
    var totalDebt : UILabel!
    var totalLoan : UILabel!

    var recalculateButton : UIButton!
    
    func setUserNameAnchors() {
        userName = UILabel()
        self.view.addSubview(userName)
        
        userName.translatesAutoresizingMaskIntoConstraints = false
        userName.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true
        userName.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        userName.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        userName.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        userName.backgroundColor = .orange
    }
    func setEMailAnchors() {
        eMail = UILabel()
        self.view.addSubview(eMail)
        
        eMail.translatesAutoresizingMaskIntoConstraints = false
        eMail.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true
        eMail.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        eMail.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        eMail.topAnchor.constraint(equalTo: self.userName.bottomAnchor, constant: betweenLabelConstant).isActive = true
        eMail.backgroundColor = .orange
    }
    

    func setFirstNameSecondNameAnchors() {
        firstNameSecondName = UILabel()
        self.view.addSubview(firstNameSecondName)
        
        firstNameSecondName.translatesAutoresizingMaskIntoConstraints = false
        firstNameSecondName.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true
        firstNameSecondName.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        firstNameSecondName.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        firstNameSecondName.topAnchor.constraint(equalTo: self.eMail.bottomAnchor, constant: betweenLabelConstant).isActive = true
        firstNameSecondName.backgroundColor = .orange
    }
    
    func setTotalDebtAnchors() {
        totalDebt = UILabel()
        self.view.addSubview(totalDebt)
        
        totalDebt.translatesAutoresizingMaskIntoConstraints = false
        totalDebt.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true
        totalDebt.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        totalDebt.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        totalDebt.topAnchor.constraint(equalTo: firstNameSecondName.bottomAnchor, constant: betweenLabelConstant).isActive = true
        totalDebt.backgroundColor = .orange
    }
    
    func setTotalLoanAnchors() {
        totalLoan = UILabel()
        self.view.addSubview(totalLoan)
        
        totalLoan.translatesAutoresizingMaskIntoConstraints = false
        totalLoan.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true
        totalLoan.leadingAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        totalLoan.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        totalLoan.topAnchor.constraint(equalTo: totalDebt.bottomAnchor, constant: betweenLabelConstant).isActive = true
        totalLoan.backgroundColor = .orange
    }
    
    func setRecalculateButton() {
        recalculateButton = UIButton()
        self.view.addSubview(recalculateButton)
        
        recalculateButton.translatesAutoresizingMaskIntoConstraints = false
        recalculateButton.heightAnchor.constraint(equalToConstant: labelHeight).isActive = true
        recalculateButton.leadingAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.leadingAnchor, constant: leadingConstant).isActive = true
        recalculateButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: trailingConstant).isActive = true
        recalculateButton.topAnchor.constraint(equalTo: totalLoan.bottomAnchor, constant: betweenLabelConstant).isActive = true
        recalculateButton.backgroundColor = .purple
        recalculateButton.setTitle("Recalculate", for: .normal)
        
        recalculateButton.showsTouchWhenHighlighted = true
    }
    
    func setView() {
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
        } else {
            self.view.backgroundColor = .white
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setUserNameAnchors()
        setEMailAnchors()
        setFirstNameSecondNameAnchors()
        setTotalDebtAnchors()
        setTotalLoanAnchors()
        setRecalculateButton()
        userName.text = "@username"
        eMail.text = "username@gmail.com"
        firstNameSecondName.text = "Ivan Ivanov"
        totalDebt.text = "50$"
        totalLoan.text = "-15.5$"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
