//
//  ContactTableViewCell.swift
//  MegaDebt
//
//  Created by Artuhay on 25.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
  
  let contactImage : UIImageView = {
    let imgView = UIImageView()
    imgView.translatesAutoresizingMaskIntoConstraints = false
    imgView.contentMode = .scaleAspectFill
    return imgView
  }()
  
  let contactName : UILabel = {
    let contN = UILabel()
    contN.translatesAutoresizingMaskIntoConstraints = false
    return contN
  }()
  
  func addConstraints() {
    addSubview(contactImage)
    addSubview(contactName)
    contactImage.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
    contactImage.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    contactImage.topAnchor.constraint(equalTo: topAnchor).isActive = true
    contactImage.widthAnchor.constraint(equalToConstant: 60).isActive = true
    contactName.leftAnchor.constraint(equalTo: contactImage.rightAnchor).isActive = true
    contactName.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    contactName.topAnchor.constraint(equalTo: topAnchor).isActive = true
    contactName.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
  }
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    addConstraints()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  

}
