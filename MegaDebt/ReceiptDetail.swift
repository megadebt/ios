//
//  ReceiptDetail.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct ReceiptDetail: Codable {
	enum State: String, Codable {
		case open
		case in_progress
		case closed
	}

	let id: String
	let date: Date
	let name: String
	let state: State
	let amount: Double
	let paid_amount: Double
	let owner: User
	let items: [Item]
	let participants: [User]
	let debts: [Debt]
	let description: String
}
