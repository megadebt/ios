//
//  ReceiptCell.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 17.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

final class ReceiptCell: UITableViewCell {

	@IBOutlet weak var debtImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var totalLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!

	var viewModel: ReceiptCellViewModel? {
		didSet {
			setupAppearence()
		}
	}

	override func layoutSubviews() {
		totalLabel.layer.cornerRadius = 5.0
		totalLabel.layer.masksToBounds = true

		let tapGR = UITapGestureRecognizer(target: self, action: #selector(didTap))
		addGestureRecognizer(tapGR)
		super.layoutSubviews()
	}

	func setupAppearence() {
		guard let viewModel = viewModel else { return }
		titleLabel.text = viewModel.title

		switch viewModel.relation {
		case .borrower(let paid, let total):
			totalLabel.text = " \(Int(paid))/\(Int(total)) ₽ "
			totalLabel.textColor = .white
			totalLabel.backgroundColor = UIColor.flatAlizarin
			debtImageView.tintColor = UIColor.flatAlizarin
		case .creditor(let paid, let total):
			totalLabel.text = " \(Int(paid))/\(Int(total)) ₽ "
			totalLabel.textColor = .white
			totalLabel.backgroundColor = UIColor.flatEmerald
			debtImageView.tintColor = UIColor.flatEmerald
		}

		if viewModel.status == .closed {
			totalLabel.textColor = .white
			totalLabel.backgroundColor = .systemGray
			titleLabel.isEnabled = false
			debtImageView.tintColor = .systemGray
		} else {
			totalLabel.isEnabled = true
			titleLabel.isEnabled = true
		}

		dateLabel.text = viewModel.date
	}

	@objc func didTap() {
		self.viewModel?.actionHanler?()
	}
}
