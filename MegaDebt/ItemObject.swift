//
//  ItemObject.swift
//  MegaDebt
//
//  Created by Artuhay on 25.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct ItemObject {
  var itemName: String
  var itemPrice: Int
  var itemQuantity: Int
  var itemMembers: [String]
  
  init() {
    self.itemName = ""
    self.itemPrice = 0
    self.itemQuantity = 0
    self.itemMembers = [String]()

  }
  
  init(name: String, price: Int, quantity: Int, members: [String]) {
    self.itemName = name
    self.itemPrice = price
    self.itemQuantity = quantity
    self.itemMembers = members
  }
}
