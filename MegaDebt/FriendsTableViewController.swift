//
//  FriendsViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class FriendsViewController: UITableViewController {
  
  //var friendsService : FriendsServiceProtocol
	

  let service : FriendsServiceProtocol = FriendsService(networkService: NetworkService(), urlResolver: URLResolver())
	var sendID: String?
  
	var friendsNames = [(mail:String, id:String, request:Bool)]()
  
  override func viewWillAppear(_ animated: Bool) {
    //self.getFriends()
  }

    override func viewDidLoad() {
      super.viewDidLoad()
      self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchFriend))
      setupRefresh()
    }
  
  @objc func searchFriend() {
    performSegue(withIdentifier: "inviteFriend", sender: nil)
  }
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "friendVCSegue" {
			let controller = segue.destination as! FriendViewController
			if let sId = sendID {
				controller.userId = sId
			}
			sendID = nil
		}
	}
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return friendsNames.count
  }
    
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath) as! FriendTableViewCell
    let fr = friendsNames[indexPath.row]
		if fr.request {
      cell.friendType = .invited
			cell.invitedID = fr.id
    } else {
      cell.friendType = .alreadyAdded
    }
    cell.friendName.text = fr.mail
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		sendID = friendsNames[indexPath.row].id
    performSegue(withIdentifier: "friendVCSegue", sender: nil)
//		service.getUserInfoById(id: friendsNames[indexPath.row].id!, completionHandler: {[weak self] result in
//			getUserID(result: "")
//		})
  }
  
  override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    self.tableView.refreshControl?.endRefreshing()
  }
  
  func updateFriendsNames(users: [User]) {
    for u in users {
			friendsNames.append((u.login, u.id, false))
    }
  }
  
  func updateFriendsRequests(invites: [Invite]) {
     for inv in invites {
      friendsNames.append((inv.sender.login, inv.sender.id, true))
     }
   }
	
  
  func updateFriendsTable(result: Result<[User], Error>) {
    switch result {
    case .success(let items):
      updateFriendsNames(users: items)
      self.tableView.reloadData()
    case .failure(let _):
      print("Failed to load friends!")
    }
    self.tableView.refreshControl?.endRefreshing()
  }
  
  func updateFriendsInvites(result:Result<[Invite], Error>) {
    switch result {
    case .success(let items):
      updateFriendsRequests(invites: items)
      self.tableView.reloadData()
    case .failure(let _):
      print("Failed to load friends!")
    }
    self.tableView.refreshControl?.endRefreshing()
  }
    
    func setupRefresh() {
      tableView.refreshControl = UIRefreshControl()
      tableView.refreshControl?.beginRefreshing()
      tableView.refreshControl?.addTarget(self, action: #selector(getFriends), for: .valueChanged)
      tableView.delegate = self
      getFriends()
    }
    
    @objc func getFriends() {
      friendsNames.removeAll()
      service.getFriends(completionHandler: {[weak self] result in
       DispatchQueue.main.async {
         self?.updateFriendsTable(result: result)
       }})
      service.getInvites(completionHandler: {
        [weak self] result in 
        DispatchQueue.main.async {
          self?.updateFriendsInvites(result: result)
        }})
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
