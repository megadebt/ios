//
//  Receipt.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 01.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct Receipt: Codable {
	let id: String
	let date: Date
	let name: String
	let description: String
	let owner: User
	let perticipants: [User]
	let items: [Item]
	let debts: [Debt]
}
