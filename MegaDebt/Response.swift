//
//  Response.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 06.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct Response<T>: Codable where T: Codable {
	let status: String
	let error_code: String?
	let error_message: String?
	let request_id: String
	let payload: T?
}
