//
//  Login.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 08.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation
struct Login: Codable {
	let login: String
}

struct Id: Codable {
  let id: String
}
