//
//  PositionDetailsAssembly.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 18.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

final class PositionDetailsAssembly {

	static func assembleWithPosition(position: ReceiptAdd.Item,
									 controller: PositionDetailsViewController) {
		let urlResolver = URLResolver()
		let networkService = NetworkService()
		let friendsService = FriendsService(networkService: networkService,
											urlResolver: urlResolver)

		let presenter = PositionDetailsPresenter()

		let interator = PositionDetailsInteractor(service: friendsService,
												  position: position,
												  output: presenter)
		presenter.interactor = interator
		presenter.output = controller
		controller.interactor = interator
	}

	static func assembleNewPosition(controller: PositionDetailsViewController) {
		let urlResolver = URLResolver()
		let networkService = NetworkService()
		let friendsService = FriendsService(networkService: networkService,
											urlResolver: urlResolver)

		let presenter = PositionDetailsPresenter()

		let interator = PositionDetailsInteractor(service: friendsService,
												  output: presenter)
		presenter.interactor = interator
		presenter.output = controller
		controller.interactor = interator
	}
}
