//
//  Debt.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 01.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct Debt: Codable {
	enum State: String, Codable {
		case open
		case pending
		case closed
	}
	let id: String
	let state: State
	let paid_date: Date
	let receipt_id: String
	let debtor: User
	let borrower: User
	let amount: Double
}
