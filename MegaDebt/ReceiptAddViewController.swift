//
//  ReceiptAddViewController.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 08.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

enum ReceiptAddCellModel {
	case textField(text: String, placeHolder: String, action: ((String) -> Void)?)
	case addPositionButton(action: (() -> Void)?)
	case saveButton(isActive: Bool, title: String, action: (() -> Void)?)
	case position(name: String,
		count: Int,
		total: Double,
		action: (() -> Void)?)
	case total(total: Double)
}

final class ReceiptAddViewController: UIViewController {

	let bag = DisposeBag()

	lazy var items = PublishSubject<[SectionModel<String, ReceiptAddCellModel>]>()

	var interactor: (ReceiptAddInteractorProtocol & PositionDetailsViewControllerDelegate)?

	let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, ReceiptAddCellModel>>(configureCell: {
		(dataSource, table, indexPath, cellViewModel) in
		switch cellViewModel {
		case let .textField(text, placeHolder, action):
			let cell = table.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
			cell.textField.placeholder = placeHolder
			cell.textField.text = text
			cell.didEndEditingHandler = action
			return cell
		case let .position(name, count, total, action):
			let cell = table.dequeueReusableCell(withIdentifier: "PositionCell") as! PositionCell
			let viewModel = PositionCellViewModel(name: name, count: count, total: total)
			cell.set(viewModel: viewModel)
			cell.actionHandler = action
			return cell
		case let .addPositionButton(action):
			let cell = table.dequeueReusableCell(withIdentifier: "AddButtonCell") as! BigButtonCell
			cell.actionHandler = action
			return cell
		case let .saveButton(isActive, title, action):
			let cell = table.dequeueReusableCell(withIdentifier: "BigButtonCell") as! SaveButtonCell
			cell.actionHandler = action
			cell.button.isEnabled = isActive
			cell.title = title
			return cell
		case let .total(total):
			let cell = table.dequeueReusableCell(withIdentifier: "TotalCell") as! TotalCell
			cell.set(total: total)
			return cell
		}
	})

	@IBOutlet weak var tableView: UITableView!

	override func viewDidLoad() {
		super.viewDidLoad()
		assemble()
		configureNavigationItem()
		configureTableView()
		startModule()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		interactor?.fetch()
	}

	func configureNavigationItem() {
		navigationItem.title = "Create Receipt"
	}

	func configureTableView() {
		dataSource.titleForHeaderInSection = { dataSource, index in
			return dataSource.sectionModels[index].model
		}

		items.bind(to: tableView.rx.items(dataSource: dataSource)).disposed(by: bag)
	}

	func assemble() {
		ReceiptAddAssembly.assemble(controller: self)
	}

	func startModule() {
		interactor?.fetch()
	}
}

extension ReceiptAddViewController: ReceiptAddPresenterOutputProtocol {
	func presentPositionDetails(position: ReceiptAdd.Item?) {
		let storyboard = UIStoryboard(name: "ReceiptsFlow", bundle: nil)
		let nav = storyboard.instantiateViewController(withIdentifier: "ReceiptDetailsNav") as! UINavigationController

		let receiptAddViewController = nav.children.first as! PositionDetailsViewController
		if let position = position {
			receiptAddViewController.setupWithPosition(position: position)
			receiptAddViewController.delegate = interactor
		} else {
			receiptAddViewController.setupNewPosition()
			receiptAddViewController.delegate = interactor
		}

		present(nav, animated: true) { [weak self] in
			self?.interactor?.fetch()
		}
	}

	func presentSections(sectionModels: [SectionModel<String, ReceiptAddCellModel>]) {
		items.onNext(sectionModels)
	}

	func presentError(error: Error) {
		presentAlertWith(error: error)
	}

	func close() {
		navigationController?.popViewController(animated: true)
	}
}
