//
//  UIViewController+Error.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 08.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

extension UIViewController {

	func presentAlertWith(error: Error) {
		let alert = UIAlertController(title: "Received error",
									  message: error.localizedDescription,
									  preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		present(alert, animated: true)
	}
}
