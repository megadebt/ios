//
//  Invite.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct Invite: Codable {
	let sender: User
}
