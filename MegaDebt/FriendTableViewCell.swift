//
//  FriendTableViewCell.swift
//  MegaDebt
//
//  Created by Artuhay on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

enum FriendType: String {
  case invited = "Invite"
  case alreadyAdded = "AlreadyAdded"
}

class FriendTableViewCell: UITableViewCell {
  
  private let service : FriendsServiceProtocol = FriendsService(networkService: NetworkService(), urlResolver: URLResolver())
  var friendType: FriendType! {
    didSet{
      switch friendType {
      case .invited:
        doesntAcceptInvite()
      case .alreadyAdded:
        acceptInviteFunc()
      case .none:
        print("Non friends type")
      }
    }
  }
  
  var invitedID : String?

  @IBOutlet weak var friendAvatar: UIImageView!
  
  @IBOutlet weak var friendName: UILabel!
  
  @IBOutlet weak var acceptInvite: UIButton!
  
  @IBOutlet weak var dismissInvite: UIButton!
  
  @IBAction func acceptFriendship(_ sender: Any) {
    if let invID = invitedID {
    service.acceptInvite(inviteId: invID, completionHandler: {[weak self] result in
      DispatchQueue.main.async {
        self?.acceptInviteFunc()
      }
    })
      print("Accept")
    }
    
  }
  
  @IBAction func denyFriendship(_ sender: Any) {
    if let invID = invitedID {
    service.declineInvite(inviteId: invID, completionHandler: {[weak self] result in  
      DispatchQueue.main.async {
        self?.doesntAcceptInvite()
      }
    })
      print("Deny")
    }
    
  }
  
  
  
  override func awakeFromNib() {
    self.frame.size.width = UIScreen.main.bounds.width
    layoutIfNeeded()
//    super.awakeFromNib()
//    friendType = FriendType(rawValue: "Invite")
//    friendType = .invited
//    switch friendType {
//    case .alreadyAdded:
//      acceptInvite.isHidden = true
//      dismissInvite.isHidden = true
//    case .invited:
//      acceptInvite.isHidden = false
//      dismissInvite.isHidden = false
//    default:
//      break
  //  }
        // Initialization code
    }
  
  func acceptInviteFunc() {
    acceptInvite.isHidden = true
    dismissInvite.isHidden = true
  }
  func doesntAcceptInvite() {
    acceptInvite.isHidden = false
    dismissInvite.isHidden = false
  }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
