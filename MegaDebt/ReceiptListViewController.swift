//
//  ReceiptsViewController.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 17.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

final class ReceiptListViewController: UIViewController, UITableViewDelegate {

	@IBOutlet weak var receiptTableView: UITableView!
	var interactor: ReceiptListInteractorProtocol?
	let bag = DisposeBag()
	let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, ReceiptCellViewModel>>(configureCell: {
		(dataSource, table, indexPath, cellViewModel) in
		guard let cell = table.dequeueReusableCell(withIdentifier: "receipt_cell") as? ReceiptCell else {
			return UITableViewCell()
		}
		cell.viewModel = cellViewModel
		return cell
	})

	let publish = PublishSubject<[SectionModel<String, ReceiptCellViewModel>]>()

	enum ScreenType {
		case active
		case history
		case unknown
	}

	var screenType: ScreenType {
		guard let controllers = self.tabBarController?.viewControllers,
			let navigtaionController = self.navigationController,
			let index = controllers.firstIndex(of: navigtaionController) else { return .unknown }
		if index == 0 {
			return .history
		}
		return .active
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		assemble()
		setupRefresh()
		setupNavigationItem()
		setupSections()
		publish.bind(to: receiptTableView.rx.items(dataSource: dataSource)).disposed(by: bag)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		receiptTableView.refreshControl?.beginRefreshing()
		reloadItems()
	}

	func assemble() {
		ReceiptListAssebmly.assemble(controller: self)
	}

	func setupRefresh() {
		receiptTableView.refreshControl = UIRefreshControl()
		receiptTableView.refreshControl?.beginRefreshing()
		receiptTableView.refreshControl?.addTarget(self, action: #selector(reloadItems), for: .valueChanged)
		receiptTableView.delegate = self
		reloadItems()
	}

	func setupNavigationItem() {
		switch screenType {
		case .active:
			navigationItem.title = "Receipts"
			navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
																target: self,
																action: #selector(createNewReceipt))
		case .history:
			navigationItem.title = "History"
		default:
			return
		}
	}

	func setupSections() {
		switch screenType {
		case .active:
			dataSource.titleForHeaderInSection = { dataSource, index in
				switch index {
				case 0:
					return "Му Receipts"
				case 1:
					return "Participated Receipts"
				default:
					return nil
				}
			}
		default:
			return
		}
	}

	@objc func createNewReceipt() {
		let storyboard = UIStoryboard(name: "ReceiptsFlow", bundle: nil)
		let receiptAddViewController = storyboard.instantiateViewController(withIdentifier: "ReceiptAddViewController")
		navigationController?.pushViewController(receiptAddViewController, animated: true)
	}

	@objc func reloadItems() {
		switch screenType {
		case .active:
			interactor?.getReceipts()
		case .history:
			interactor?.getReceiptsHistory()
		default:
			return
		}
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		tableView.refreshControl?.endRefreshing()
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		(tableView.cellForRow(at: indexPath) as? ReceiptCell)?.viewModel?.actionHanler?()
		tableView.deselectRow(at: indexPath, animated: true)
	}
}

extension ReceiptListViewController: ReceiptListPresenterOutput {

	func presentReceipts(myReceipts: [ReceiptCellViewModel], participatedReceipts: [ReceiptCellViewModel]) {
		receiptTableView.refreshControl?.endRefreshing()
		let myItems = SectionModel<String, ReceiptCellViewModel>(model: "0", items: myReceipts)
		let participatedItems = SectionModel<String, ReceiptCellViewModel>(model: "1", items: participatedReceipts)
		publish.onNext([myItems, participatedItems])
	}

	func presentHistory(receipts: [ReceiptCellViewModel]) {
		receiptTableView.refreshControl?.endRefreshing()
		let items = SectionModel<String, ReceiptCellViewModel>(model: "0", items: receipts)
		publish.onNext([items])
	}

	func presentError(error: Error) {
		receiptTableView.refreshControl?.endRefreshing()
		presentAlertWith(error: error)
	}

	func presentReceiptDetails(receiptId: String) {
		let storyboard = UIStoryboard(name: "ReceiptsFlow", bundle: nil)
		let receiptDetails = storyboard.instantiateViewController(withIdentifier: "ReceiptDetails") as! ReceiptDetailsViewController
		receiptDetails.receiptId = receiptId
		navigationController?.pushViewController(receiptDetails, animated: true)
	}
}
