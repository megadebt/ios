//
//  ReceiptAdd.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

struct ReceiptAdd: Codable {

	struct Item: Codable {
		var name: String
		var quantity: Int
		var price: Double
		var participants: [String]
	}

	var name: String
	var date: Date
	var description: String
	var items: [Item]
	var participants: [String]
}

struct ReceiptAddResponse: Codable {
	let receipt_id: String
}
