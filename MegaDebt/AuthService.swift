//
//  AuthService.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation
import GoogleSignIn
import Firebase

final class AuthService {

	static let shared = AuthService()

	public var token: String? {
		return UserDefaults.standard.string(forKey: "Token")
	}
	public var userID: String? {
		return UserDefaults.standard.string(forKey: "UserID")
	}

	func didSignIn() {
		let urlResoler = URLResolver()
		let networkService = NetworkService()
		let userId = String(userID!.split(separator: "@").first!)
		let login = Login(login: userId)
		networkService.requestDefault(path: urlResoler.resolveUrl(with: "user/register"),
									  parameters: login,
									  method: .post,
									  completionHandler: { (result: Result<String, Error>) in
			print(result)
		})
	}
}
