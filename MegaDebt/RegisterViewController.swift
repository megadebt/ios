//
//  RegisterViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 30.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {
  
  var profileVCDelegate : ProfileViewControllerDelegate?

	@IBOutlet weak var eMail: UITextField!

	@IBOutlet weak var password: UITextField!

	@IBOutlet weak var confirmPassword: UITextField!
  
  @IBOutlet weak var fullName: UITextField!
  
  @IBOutlet weak var login: UITextField!
  
  @IBAction func signUp(_ sender: Any) {
		Auth.auth().createUser(withEmail: eMail.text!, password: password.text!, completion: { [weak self] authResult, error in
			guard let self = self else { return }

			guard error == nil else {
				self.presentAlertWith(error: error!)
				return
			}
			if let authResult = authResult,
				let userName = authResult.user.email {
				UserDefaults.standard.set(true, forKey: "isLogIn")
				UserDefaults.standard.setValue(userName, forKey: "UserID")
				authResult.user.getIDToken { token, error in
					guard let token = token else { return }
					UserDefaults.standard.setValue(token, forKey: "Token")
					AuthService.shared.didSignIn()
					self.navigationController?.popViewController(animated: true)
				}
			}
		})
	}

	override func viewDidLoad() {
		super.viewDidLoad()
    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeKeyboard)))
		// Do any additional setup after loading the view.
	}
  
  @objc func closeKeyboard() {
    self.view.endEditing(true)
  }


	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/

}
