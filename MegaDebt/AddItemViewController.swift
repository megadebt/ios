//
//  AddItemViewController.swift
//  MegaDebt
//
//  Created by Artuhay on 25.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {
  
  var currenItemObject: ItemObject!
  
  var createMeetingDelegate : MeetingViewControllerDelegate?
  
  var contacts : [String] = ["Ивантус", "Азиат", "Фелициус", "Артухай", "Жога", "Некит", "Аминат"]
  
  let tableCellId = "ContactCell"
  
  let itemName: CustomTextField = {
    let tf = CustomTextField()
    tf.placeholder = "Input item name"
    tf.returnKeyType = .done
    tf.translatesAutoresizingMaskIntoConstraints = false
    tf.layer.borderColor = UIColor.red.cgColor
    return tf
  }()
  
  let price: CustomTextField = {
    let tf = CustomTextField()
    
    tf.placeholder = "Input price of item"
    tf.returnKeyType = .done
    tf.translatesAutoresizingMaskIntoConstraints = false
    tf.layer.borderColor = UIColor.red.cgColor
    tf.keyboardType = .decimalPad
    return tf
  }()

  let number: CustomTextField = {
    let tf = CustomTextField()
    tf.placeholder = "Input number of items"
    tf.returnKeyType = .done
    tf.translatesAutoresizingMaskIntoConstraints = false
    tf.layer.borderColor = UIColor.red.cgColor
    tf.keyboardType = .numberPad
    return tf
  }()
  
  let contactsTableView: UITableView = {
    let table = UITableView()
    table.register(ContactTableViewCell.self, forCellReuseIdentifier: "ContactCell")
    table.translatesAutoresizingMaskIntoConstraints = false
    table.allowsMultipleSelection = true
    return table
  }()
  
  let sendButton : UIButton = {
    let button = UIButton()
    button.translatesAutoresizingMaskIntoConstraints = false
    button.backgroundColor = .cyan
    button.setTitle("Send", for: .normal)
    button.showsTouchWhenHighlighted = true
    return button
  }()
  
  func addConstraintsForAllTF() {
    let safeArea = view.safeAreaLayoutGuide
    view.addSubview(itemName)
    view.addSubview(price)
    view.addSubview(number)
    view.addSubview(contactsTableView)
    view.addSubview(sendButton)
    itemName.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
    itemName.heightAnchor.constraint(equalToConstant: 50).isActive = true
    itemName.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
    itemName.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
    itemName.tag = 1
    itemName.delegate = self
    
    price.topAnchor.constraint(equalTo: itemName.bottomAnchor).isActive = true
    price.heightAnchor.constraint(equalToConstant: 50).isActive = true
    price.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
    price.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
    price.tag = 2
    price.delegate = self
    
    number.topAnchor.constraint(equalTo: price.bottomAnchor).isActive = true
    number.heightAnchor.constraint(equalToConstant: 50).isActive = true
    number.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
    number.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
    number.tag = 3
    number.delegate = self
    
    sendButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    sendButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
    sendButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
    sendButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
    sendButton.addTarget(self, action: #selector(setPosition), for: .touchUpInside)
    
    contactsTableView.topAnchor.constraint(equalTo: number.bottomAnchor).isActive = true
    contactsTableView.bottomAnchor.constraint(equalTo: sendButton.topAnchor).isActive = true
    contactsTableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
    contactsTableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true

  }
  
  @objc func setPosition() {
    if let indexes = contactsTableView.indexPathsForSelectedRows {
      for ind in indexes {
        currenItemObject.itemMembers.append(contacts[ind.row])
      }
    }
      createMeetingDelegate?.addMeetingItems(item: currenItemObject)
    dismiss(animated: true, completion: nil)
  }
  
  func setupView() {
    let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
    tap.cancelsTouchesInView = false
    self.view.addGestureRecognizer(tap)

    if #available(iOS 13.0, *) {
      self.view.backgroundColor = .systemBackground
    } else {
      // Fallback on earlier versions
    }
  }
  
  @objc func dismissKeyboard() {
    self.view.endEditing(true)
  }

  
    override func viewDidLoad() {
      super.viewDidLoad()
      setupView()
      addConstraintsForAllTF()
      contactsTableView.delegate = self
      contactsTableView.dataSource = self
      
      currenItemObject = ItemObject()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddItemViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return contacts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: tableCellId, for: indexPath) as! ContactTableViewCell
    cell.contactName.text = contacts[indexPath.row]
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let selectedCell = tableView.cellForRow(at: indexPath)
    selectedCell?.accessoryType = .checkmark
    //print(indexPath.row)
  }
  
  func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    let selectedCell = tableView.cellForRow(at: indexPath)
    selectedCell?.accessoryType = .none
  }
  
  
}

extension AddItemViewController: UITextFieldDelegate {
  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField.tag == 1 {
      currenItemObject.itemName = textField.text ?? ""
    }
    if textField.tag == 2 {
      if let text = textField.text{
        currenItemObject.itemPrice = Int(text) ?? 0
      }
    }
    if textField.tag == 3 {
      if let text = textField.text{
        currenItemObject.itemQuantity = Int(text) ?? 0
      }
    }
  }
}
