//
//  PositionCell.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 15.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

struct PositionCellViewModel {
	let name: String
	let count: Int
	let total: Double
}

final class PositionCell: UITableViewCell {
	@IBOutlet private weak var nameLabel: UILabel!
	@IBOutlet private weak var countLabel: UILabel!
	@IBOutlet private weak var totalLabel: UILabel!

	var actionHandler: (() -> Void)?

	func set(viewModel: PositionCellViewModel) {
		nameLabel.text = viewModel.name
		countLabel.text = "Quantity: \(viewModel.count)"
		totalLabel.text = String(format: "Total: %.2f ₽", viewModel.total)
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap)))
	}

	@objc private func didTap() {
		actionHandler?()
	}
}
