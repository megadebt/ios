//
//  PositionDetailsPresenter.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 18.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import RxDataSources

protocol PositionDetailsPresenterOutputProtocol: AnyObject {
	func present(sectionModels: [SectionModel<String, PositionCellModel>], isValid: Bool)

	func present(error: Error)

	func close()
}

final class PositionDetailsPresenter: PositionDetailsInteractorOutputProtocol {

	weak var output: PositionDetailsPresenterOutputProtocol?
	weak var interactor: PositionDetailsInteractorProtocol?

	func present(position: ReceiptAdd.Item,
				 users: [User],
				 nameDictionary: [String : String],
				 avatarDictionary: [String : UIImage],
				 isValid: Bool) {

		let propertiesSection = makePropertiesSection(position)
		let usersSection = makeParticipantSection(users: users,
												  selectedUsersId: position.participants,
												  nameDictionary: nameDictionary,
												  avatarDictionary: avatarDictionary)

		output?.present(sectionModels: [propertiesSection, usersSection], isValid: isValid)
	}

	private func makePropertiesSection(_ position: ReceiptAdd.Item) -> SectionModel<String, PositionCellModel> {
		let nameItem = PositionCellModel.textField(placeholder: "Position Name",
												   value: position.name,
												   keyboardType: .asciiCapable)
		{
			[weak self] newName in
			self?.interactor?.set(name: newName)
		}

		let quantityItem = PositionCellModel.textField(placeholder: "Quantity",
													   value: String(position.quantity),
													   keyboardType: .asciiCapableNumberPad)
		{
			[weak self] newQuantity in
			if let count = Int(newQuantity) {
				self?.interactor?.set(quantity: count)
			}
		}

		let priceItem = PositionCellModel.textField(placeholder: "Price",
													value: String(position.price),
													keyboardType: .decimalPad)
		{
			[weak self] newPrice in
			if let price = Double(newPrice) {
				self?.interactor?.set(price: price)
			}
		}

		return SectionModel(model: "Properties", items: [nameItem, quantityItem, priceItem])
	}

	private func makeParticipantSection(users: [User],
										selectedUsersId: [String],
										nameDictionary: [String: String],
										avatarDictionary: [String: UIImage]) -> SectionModel<String, PositionCellModel> {
		let items = users.map { user in
			return PositionCellModel.friend(name: nameDictionary[user.id] ?? "UNKNOWN",
									 avatar: avatarDictionary[user.id],
									 isSelected: selectedUsersId.contains(user.id),
									 action: { [weak self] isSelected in
										if isSelected {
											self?.interactor?.addParticipant(id: user.id)
										} else {
											self?.interactor?.deleteParticipant(id: user.id)
										}
			})
		}

		return SectionModel(model: "Participants", items: items)
	}

	func present(error: Error) {
		output?.present(error: error)
	}

	func close() {
		output?.close()
	}
}
