//
//  Item.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 01.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

struct Item: Codable {
	let id: String
	let name: String
	let quantity: Int
	let price: Int
	let participants: [User]
}
