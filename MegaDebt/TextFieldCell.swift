//
//  TextFieldCell.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 08.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

final class TextFieldCell: UITableViewCell {
	@IBOutlet weak var textField: UITextField!

	var didEndEditingHandler: ((String) -> Void)?
}

extension TextFieldCell: UITextFieldDelegate {
	func textFieldDidEndEditing(_ textField: UITextField) {
		if let text = textField.text {
			didEndEditingHandler?(text)
		}
	}
}
