//
//  PositionDetailsViewController.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 18.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

enum PositionCellModel {
	case textField(placeholder: String,
		value: String,
		keyboardType: UIKeyboardType,
		action: ((String) -> Void)?)
	case friend(name: String,
		avatar: UIImage?,
		isSelected: Bool,
		action: ((Bool) -> Void)?)
}

protocol PositionDetailsViewControllerDelegate: AnyObject {
	func didAddPosition(position: ReceiptAdd.Item)

	func didChangePosition(position: ReceiptAdd.Item)
}

final class PositionDetailsViewController: UIViewController {

	@IBOutlet weak var saveButton: UIBarButtonItem!
	@IBOutlet weak var tableView: UITableView!
	weak var delegate: PositionDetailsViewControllerDelegate?

	var isInEditMode = false

	var interactor: PositionDetailsInteractorProtocol?

	let bag = DisposeBag()
	lazy var items = PublishSubject<[SectionModel<String, PositionCellModel>]>()

	let datasource = RxTableViewSectionedReloadDataSource<SectionModel<String, PositionCellModel>>(configureCell: {
		(dataSource, table, indexPath, cellViewModel) in
		switch cellViewModel {
		case let .friend(name, avatar, isSelected, action):
			let cell = table.dequeueReusableCell(withIdentifier: "FriendSelectCell") as! FriendSelectCell
			cell.nameLabel.text = name
			if let avatar = avatar {
				cell.avatarImageView.image = avatar
			}
			cell.isSelected = isSelected
			cell.actionBlock = action
			return cell
		case let .textField(placeholder, value, keyboardType, action):
			let cell = table.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
			cell.textField.placeholder = placeholder
			cell.textField.text = value
			cell.textField.keyboardType = keyboardType
			cell.didEndEditingHandler = action
			return cell
		}
	})

	func setupWithPosition(position: ReceiptAdd.Item) {
		isInEditMode = true
		PositionDetailsAssembly.assembleWithPosition(position: position,
													 controller: self)
	}

	func setupNewPosition() {
		PositionDetailsAssembly.assembleNewPosition(controller: self)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		setupTableView()
		interactor?.start()
	}

	func setupTableView() {
		items.bind(to: tableView.rx.items(dataSource: datasource)).disposed(by: bag)

		datasource.titleForHeaderInSection = { dataSource, index in
			return dataSource.sectionModels[index].model
		}

		tableView.refreshControl = UIRefreshControl()
		tableView.refreshControl?.beginRefreshing()
		tableView.refreshControl?.addTarget(self, action: #selector(fetchItems), for: .valueChanged)
	}

	@IBAction func saveButtonTap(_ sender: Any) {
		guard let position = interactor?.getPositionModel() else { return }
		if isInEditMode {
			delegate?.didChangePosition(position: position)
		} else {
			delegate?.didAddPosition(position: position)
		}
		dismiss(animated: true, completion: nil)
	}

	@IBAction func cancelButtonTap(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}

	@objc func fetchItems() {
		interactor?.start()
	}

	func setSaveAvailable(available: Bool) {
		saveButton.isEnabled = available
	}
}

extension PositionDetailsViewController: PositionDetailsPresenterOutputProtocol {
	func present(sectionModels: [SectionModel<String, PositionCellModel>], isValid: Bool) {
		setSaveAvailable(available: isValid)
		tableView.refreshControl?.endRefreshing()
		items.onNext(sectionModels)
	}

	func present(error: Error) {
		tableView.refreshControl?.endRefreshing()
		presentAlertWith(error: error)
	}

	func close() {
		dismiss(animated: true, completion: nil)
	}
}
