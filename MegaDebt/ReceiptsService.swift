//
//  ReceiptsService.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 01.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Alamofire

protocol ReceiptsServiceProtocol {
	func getReceipts(completionHandler: @escaping (Result<[ReceiptLight], Error>) -> Void)
	func getReceiptsHistory(completionHandler: @escaping (Result<[ReceiptLight], Error>) -> Void)
	func getReceipt(receiptId: String, completionHandler: @escaping (Result<ReceiptDetail, Error>) -> Void)
	func addReceipt(receipt: ReceiptAdd, completionHandler: @escaping (Result<ReceiptAddResponse, Error>) -> Void)
	func closeReceipt(receiptId: String, completionHandler: @escaping (Result<Response<String>, Error>) -> Void)
	func closeDebpt(debptId: String, completionHandler: @escaping (Result<Response<String>, Error>) -> Void)
	func changeDebptState(debptId: String, completionHandler: @escaping (Result<Response<String>, Error>) -> Void)
}

final class ReceiptsService: ReceiptsServiceProtocol {

	let networkService: NetworkService
	let urlResolver: URLResolverProtocol

	init(networkService: NetworkService,
		 urlResolver: URLResolverProtocol) {
		self.networkService = networkService
		self.urlResolver = urlResolver
	}

	func getReceipts(completionHandler: @escaping (Result<[ReceiptLight], Error>) -> Void) {
		networkService.requestDefault(path: getReceiptsEndpoint(),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.get) { (result: Result<[ReceiptLight], Error>) in
			completionHandler(result)
		}
	}

	func getReceiptsHistory(completionHandler: @escaping (Result<[ReceiptLight], Error>) -> Void) {
		networkService.requestDefault(path: getReceiptsHistoryEndpoint(),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.get) { (result: Result<[ReceiptLight], Error>) in
			completionHandler(result)
		}
	}

	func getReceipt(receiptId: String, completionHandler: @escaping (Result<ReceiptDetail, Error>) -> Void) {
		networkService.requestDefault(path: getReceiptEndpoint(id: receiptId),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.get) { (result: Result<ReceiptDetail, Error>) in
			completionHandler(result)
		}
	}

	func addReceipt(receipt: ReceiptAdd, completionHandler: @escaping (Result<ReceiptAddResponse, Error>) -> Void) {
		networkService.requestDefault(path: addReceiptEndpoint(),
							   parameters: receipt,
							   method: HTTPMethod.post) { (result: Result<ReceiptAddResponse, Error>) in
			completionHandler(result)
		}
	}

	func closeReceipt(receiptId: String, completionHandler: @escaping (Result<Response<String>, Error>) -> Void) {
		networkService.requestRaw(path: closeReceiptEndpoint(id: receiptId),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.post) { (result: Result<Response<String>, Error>) in
			completionHandler(result)
		}
	}

	func closeDebpt(debptId: String, completionHandler: @escaping (Result<Response<String>, Error>) -> Void) {
		networkService.requestRaw(path: closeDebptEndpoint(id: debptId),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.post) { (result: Result<Response<String>, Error>) in
			completionHandler(result)
		}
	}

	func changeDebptState(debptId: String, completionHandler: @escaping (Result<Response<String>, Error>) -> Void) {
		networkService.requestRaw(path: changeDebptEndpoint(id: debptId),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.put) { (result: Result<Response<String>, Error>) in
			completionHandler(result)
		}
	}
}

extension ReceiptsService {

	func getReceiptsEndpoint() -> String {
		return urlResolver.resolveUrl(with: "user/receipts/")
	}

	func getReceiptsHistoryEndpoint() -> String {
		return urlResolver.resolveUrl(with: "user/receipts/history")
	}

	func getReceiptEndpoint(id: String) -> String {
		return urlResolver.resolveUrl(with: "user/receipts/\(id)")
	}

	func addReceiptEndpoint() -> String {
		return urlResolver.resolveUrl(with: "user/receipts/add")
	}

	func closeReceiptEndpoint(id: String) -> String {
		return urlResolver.resolveUrl(with: "user/receipts/\(id)/close")
	}

	func closeDebptEndpoint(id: String) -> String {
		return urlResolver.resolveUrl(with: "user/debts/\(id)/close")
	}

	func changeDebptEndpoint(id: String) -> String {
		return urlResolver.resolveUrl(with: "user/debts/\(id)/state")
	}
}
