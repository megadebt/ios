//
//  UrlResolver.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 01.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation

protocol URLResolverProtocol {
	func resolveUrl(with additionalPath: String) -> String;
}

struct URLResolver: URLResolverProtocol {
	func resolveUrl(with additionalPath: String) -> String {
		guard let serverApiPath = Bundle.main.object(forInfoDictionaryKey: "Server URL") as? String else {
			return ""
		}
		return "\(serverApiPath)\(additionalPath)"
	}
}
