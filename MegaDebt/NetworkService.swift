//
//  NetworkService.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkServiceProtocol {
	
	func requestDefault<T: Codable, J: Codable>(path: String,
											 parameters:J?,
											 method: HTTPMethod,
											 completionHandler: @escaping (Result<T, Error>) -> Void)

	func requestRaw<T: Codable, J: Codable>(path: String,
													parameters:J?,
													method: HTTPMethod,
													completionHandler: @escaping (Result<T, Error>) -> Void)
}

final class NetworkService: NetworkServiceProtocol {

	enum NetworkServiceError: String, Error {
		case payloadNil
		case noToken
	}


	func requestDefault<T: Codable, J: Codable>(path: String,
											 parameters:J?,
											 method: HTTPMethod = .get,
											 completionHandler: @escaping (Result<T, Error>) -> Void) {
		guard let token = AuthService.shared.token else {
			completionHandler(.failure(NetworkServiceError.noToken))
			return
		}
		let header = HTTPHeader(name: "MD-User-Token", value: AuthService.shared.token ?? "")
		let parameterEncoder = JSONParameterEncoder()
		parameterEncoder.encoder.dateEncodingStrategy = .custom({ (date, encoder) throws in
			var container = encoder.singleValueContainer()
			let seconds: UInt = UInt(date.timeIntervalSince1970)
			try container.encode(seconds)
		})
		let decoder = JSONDecoder()
		decoder.dateDecodingStrategy = .secondsSince1970
		AF.request(path, method: method,
				   parameters: parameters,
				   encoder: parameterEncoder,
				   headers: HTTPHeaders([header]),
				   interceptor: nil)
			.validate(statusCode: 200..<600).responseDecodable(of: Response<T>.self, queue: DispatchQueue.global(),
			decoder: decoder) { response in
				do {
					let result = try response.result.get()
					if let payload = result.payload {
						completionHandler(.success(payload))
						return
					}
					completionHandler(.failure(NetworkServiceError.payloadNil))
				}
				catch let error as AFError {
					completionHandler(.failure(error))
				}
				catch let error {
					completionHandler(.failure(error))
				}
		}.resume()
	}

	func requestRaw<T: Codable, J: Codable>(path: String,
											 parameters:J?,
											 method: HTTPMethod = .get,
											 completionHandler: @escaping (Result<T, Error>) -> Void) {
		guard let token = AuthService.shared.token else {
			completionHandler(.failure(NetworkServiceError.noToken))
			return
		}
		let header = HTTPHeader(name: "MD-User-Token", value: AuthService.shared.token ?? "")
		let parameterEncoder = JSONParameterEncoder()
		parameterEncoder.encoder.dateEncodingStrategy = .custom({ (date, encoder) throws in
			var container = encoder.singleValueContainer()
			let seconds: UInt = UInt(date.timeIntervalSince1970)
			try container.encode(seconds)
		})
		let decoder = JSONDecoder()
		decoder.dateDecodingStrategy = .secondsSince1970
		AF.request(path, method: method,
				   parameters: parameters,
				   encoder: parameterEncoder,
				   headers: HTTPHeaders([header]),
				   interceptor: nil)
			.validate(statusCode: 200..<300).responseDecodable(of: T.self, queue: DispatchQueue.global(),
															   decoder: decoder) { response in
				do {
					let result = try response.result.get()
					completionHandler(.success(result))
				}
				catch let error as AFError {
					completionHandler(.failure(error))
				}
				catch let error {
					completionHandler(.failure(error))
				}
		}.resume()
	}
}
