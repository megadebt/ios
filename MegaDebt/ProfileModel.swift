//
//  ProfileModel.swift
//  MegaDebt
//
//  Created by Artuhay on 31.05.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

class ProfileModel {
  var name: String
  var email: String
  var username: String?
  var avatar: UIImage?
  var rating : Float?
  
  init(name: String, email: String) {
    self.name = name
    self.email = email
  }
  
  init(name: String, email: String, avatar: UIImage) {
    self.name = name
    self.email = email
    self.avatar = avatar
   }
}
