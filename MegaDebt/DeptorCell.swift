//
//  DeptorCell.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 18.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

final class DebtorCell: UITableViewCell {
	@IBOutlet weak var avatarImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var stateLabel: UILabel!
	@IBOutlet weak var debtAmoutLabel: UILabel!
	@IBOutlet weak var debtChangeStateButton: UIButton!

	var changeStateHandler: (() -> Void)? {
		didSet {
			if changeStateHandler == nil {
				debtChangeStateButton.isEnabled = false
			}
		}
	}

	@IBAction func debtChangeStateButtonDidTap(_ sender: Any) {
		changeStateHandler?()
	}
}

