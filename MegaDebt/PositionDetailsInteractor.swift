//
//  PositionDetailsInteractor.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 18.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import UIKit

protocol PositionDetailsInteractorProtocol: AnyObject {

	func addParticipant(id: String)

	func deleteParticipant(id: String)

	func set(name: String)

	func set(price: Double)

	func set(quantity: Int)

	func start()

	func getPositionModel() -> ReceiptAdd.Item


	func presentIfReady()
}

protocol PositionDetailsInteractorOutputProtocol: AnyObject {
	func present(position: ReceiptAdd.Item,
				 users: [User],
				 nameDictionary: [String: String],
				 avatarDictionary: [String: UIImage],
				 isValid: Bool)

	func present(error: Error)

	func close()
}

final class PositionDetailsInteractor: PositionDetailsInteractorProtocol {

	private var position: ReceiptAdd.Item {
		didSet {
			presentIfReady()
		}
	}

	private var currentUser: User? {
		didSet {
			presentIfReady()
		}
	}

	private var friends: [User]? {
		didSet {
			presentIfReady()
		}
	}

	let output: PositionDetailsInteractorOutputProtocol

	let service: FriendsServiceProtocol


	init(service: FriendsServiceProtocol,
		 output: PositionDetailsInteractorOutputProtocol) {
		position = ReceiptAdd.Item(name: "",
								   quantity: 1,
								   price: 100.0,
								   participants: [])
		self.service = service
		self.output = output
	}

	init(service: FriendsServiceProtocol,
		 position: ReceiptAdd.Item,
		 output: PositionDetailsInteractorOutputProtocol) {
		self.service = service
		self.position = position
		self.output = output
	}

	func start() {
		service.getUserInfo { [weak self] result in
			do {
				let user = try result.get()
				self?.currentUser = user.commonUser()
			} catch let error {
				DispatchQueue.main.async {
					self?.output.present(error: error)
				}
			}
		}

		service.getFriends { [weak self] result in
			do {
				let friends = try result.get()
				self?.friends = friends
			} catch let error {
				DispatchQueue.main.async {
					self?.output.present(error: error)
				}
			}
		}
	}

	func addParticipant(id: String) {
		if position.participants.firstIndex(of: id) == nil {
			position.participants.append(id)
		}
	}

	func deleteParticipant(id: String) {
		if position.participants.contains(id),
			let index = position.participants.firstIndex(of: id) {
			position.participants.remove(at: index)
		}
	}

	func set(name: String) {
		position.name = name
	}

	func set(price: Double) {
		if price >= 0.0 {
			position.price = price
		}
	}

	func set(quantity: Int) {
		if quantity >= 0 {
			position.quantity = quantity
		}
	}

	func getPositionModel() -> ReceiptAdd.Item {
		return position
	}

	func presentIfReady() {
		guard let currentUser = currentUser,
			let friends = friends else { return }

		let users = [currentUser] + friends

		let namesDictionary = users.map { ($0.id, $0.display_name.isEmpty ? $0.mail : $0.display_name) }
			.reduce(into: [:]) { $0[$1.0] = $1.1 }

		let avatarKeyValues: [(String, UIImage)] = users.compactMap { user in
			let id = user.id

			guard let url = URL(string: user.photo_url),
				let image = try? UIImage(data: Data(contentsOf: url)) else {
				 return nil
			}
			return (id, image)
		}

		let avatarDicitionary = Dictionary(uniqueKeysWithValues: avatarKeyValues)

		let isValid = !position.participants.isEmpty &&
			position.name.count > 1 &&
			position.price > 0.0 &&
			position.quantity >= 1


		output.present(position: position,
						users: users,
						nameDictionary: namesDictionary,
						avatarDictionary: avatarDicitionary,
						isValid: isValid)
	}
}
