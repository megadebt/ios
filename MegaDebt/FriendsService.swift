//
//  FriendsService.swift
//  MegaDebt
//
//  Created by Dmitry Zhoga on 07.06.2020.
//  Copyright © 2020 misis. All rights reserved.
//

import Foundation
import Alamofire

protocol FriendsServiceProtocol {
	func getFriends(completionHandler: @escaping (Result<[User], Error>) -> Void)
	func addFriend(id: String, completionHandler: @escaping (Result<Response<String>, Error>) -> Void)
	func declineInvite(inviteId: String, completionHandler: @escaping (Result<String, Error>) -> Void)
	func getInvites(completionHandler: @escaping (Result<[Invite], Error>) -> Void)
	func acceptInvite(inviteId: String, completionHandler: @escaping (Result<String, Error>) -> Void)
	func getUserInfo(completionHandler: @escaping (Result<ExtendedUser, Error>) -> Void)
	func getUserInfoById(id:String, completionHandler: @escaping (Result<User, Error>) -> Void)
	func getUserInfoByLogin(invitedLogin: String, completionHandler: @escaping (Result<[User], Error>) -> Void)
}

final class FriendsService: FriendsServiceProtocol {

	let networkService: NetworkService
	let urlResolver: URLResolverProtocol

	init(networkService: NetworkService,
		 urlResolver: URLResolverProtocol) {
		self.networkService = networkService
		self.urlResolver = urlResolver
	}

	func getFriends(completionHandler: @escaping (Result<[User], Error>) -> Void) {
		networkService.requestDefault(path: getFriendsEndpoint(),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.get) { (result: Result<[User], Error>) in
			completionHandler(result)
		}
	}

	func addFriend(id: String, completionHandler: @escaping (Result<Response<String>, Error>) -> Void) {
		networkService.requestRaw(path: addFrinedEndpoint(),
                              parameters: Id(id:id),
							   method: HTTPMethod.post) { (result: Result<Response<String>, Error>) in
			completionHandler(result)
		}
	}

	func getInvites(completionHandler: @escaping (Result<[Invite], Error>) -> Void) {
		networkService.requestDefault(path: getInvitesEndpoint(),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.get) { (result: Result<[Invite], Error>) in
			completionHandler(result)
		}
	}

	func acceptInvite(inviteId: String, completionHandler: @escaping (Result<String, Error>) -> Void) {
		networkService.requestDefault(path: acceptInviteEndpoint(id: inviteId),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.post) { (result: Result<String, Error>) in
			completionHandler(result)
		}
	}
  
  func declineInvite(inviteId: String, completionHandler: @escaping (Result<String, Error>) -> Void) {
    networkService.requestDefault(path: declineInviteEndpoint(id: inviteId),
                 parameters: Optional<String>.none,
                 method: HTTPMethod.post) { (result: Result<String, Error>) in
      completionHandler(result)
    }
  }

	func getUserInfo(completionHandler: @escaping (Result<ExtendedUser, Error>) -> Void) {
		networkService.requestDefault(path: getUserInfoEndpoint(),
							   parameters: Optional<String>.none,
							   method: HTTPMethod.get) { (result: Result<ExtendedUser, Error>) in
			completionHandler(result)
		}
	}
	func getUserInfoById(id:String, completionHandler: @escaping (Result<User, Error>) -> Void) {
    networkService.requestDefault(path: getUserInfoByIdEndpoint(id: id),
                 parameters: Optional<String>.none,
                 method: HTTPMethod.get) { (result: Result<User, Error>) in
      completionHandler(result)
    }
  }
  
  func getUserInfoByLogin(invitedLogin: String, completionHandler: @escaping (Result<[User], Error>) -> Void) {
    networkService.requestDefault(path: getUserInfoByLoginEndpoint(),
                 parameters: Login(login: invitedLogin),//"{ \"login\": \"\(invitedLogin)\" }",
                 method: HTTPMethod.post) { (result: Result<[User], Error>) in
      completionHandler(result)
    }
  }
}

extension FriendsService {

	func getFriendsEndpoint() -> String {
		return urlResolver.resolveUrl(with: "social/friends/")
	}

	func addFrinedEndpoint() -> String {
		return urlResolver.resolveUrl(with: "social/friends/add")
	}

	func getInvitesEndpoint() -> String {
		return urlResolver.resolveUrl(with: "social/friends/requests")
	}

	func acceptInviteEndpoint(id: String) -> String {
		return urlResolver.resolveUrl(with: "social/friends/requests/\(id)/approve")
	}
  
  func declineInviteEndpoint(id: String) -> String {
    return urlResolver.resolveUrl(with: "social/friends/requests/\(id)/decline")
  }

	func getUserInfoEndpoint() -> String {
		return urlResolver.resolveUrl(with: "social/info")
	}
  
  func getUserInfoByLoginEndpoint() -> String {
    return urlResolver.resolveUrl(with: "social/info/login")
  }
	func getUserInfoByIdEndpoint(id:String) -> String {
    return urlResolver.resolveUrl(with: "social/info/about/\(id)")
  }
  
  func changeUserInfoByToken() -> String {
    return urlResolver.resolveUrl(with: "social/info/login/edit")
  }
    
}
