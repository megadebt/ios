# MARK: - Megadept

### Build instruction
1. `brew install carthage` 
2. `carthage update --platform ios`
3. Build in `Xcode 11.3.1`

### Dependencies
1. Alamofire
2. RxSwift
3. RxDataSources

### Architecture
![alt text](Readme\ Resources/architecture.png)